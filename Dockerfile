FROM nginxinc/nginx-unprivileged:latest

EXPOSE 80

COPY dist/synergy-ui/* /usr/share/nginx/html/
