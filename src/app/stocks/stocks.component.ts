import { Component, OnInit } from '@angular/core';
import { Stock } from '../model/stock';
import { ActivatedRoute, Router } from '@angular/router';
import { WebService } from '../service/web.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  stock: Stock;
  ticker: string;
  stocksList: Stock[];

  constructor(private route: ActivatedRoute, private router: Router, private webService: WebService) { }
  
  ngOnInit() {
    this.getStocks();
  }

  private getStocks() {
    this.webService.findStocks().subscribe(data => {
      this.stocksList = data;
    });
  }

  onSubmit() {
    this.stock = new Stock(this.ticker);
    console.log("ticker from form: " + this.ticker)
    this.webService.save(this.stock).subscribe(
      result => {
        console.log("Form entry: " + JSON.stringify(result.ticker))
        console.log("this.stock: " + JSON.stringify(this.stock))
      }
    );
  }

  gotoStockList(){
    this.router.navigate(['/trades'])
  }

}
