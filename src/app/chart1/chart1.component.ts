import { Component, OnInit } from '@angular/core';
import { WebService } from '../service/web.service';

@Component({
  selector: 'app-chart1',
  templateUrl: './chart1.component.html',
  styleUrls: ['./chart1.component.css']
})
export class Chart1Component implements OnInit {

  constructor(private webService: WebService) { }
  trades;
  aaplPrices;
  msftPrices;
  googPrices;
  nscPrices;
  averagePricesArray: any = []
  dataPoints: any = [1];
  plotAaplDates: any = [];
  plotAaplPrice: any = [];
  plotMsftDates: any = [];
  plotMsftPrice: any = [];
  plotGoogDates: any = [];
  plotGoogPrice: any = [];

  ngOnInit(){
    this.updateTrades();
    this.getAaplPrices();
    this.getMsftPrices();
    this.getGoogPrices();
    this.getNSCPrices();
  }

  private updateTrades() {
    this.webService.findAll()
    .subscribe(data => {
      this.dataPoints = data;
      this.fillArray();
    });
  }

  private getAaplPrices(){
    this.webService.getPrices("AAPL", 1)
      .subscribe(result => {
        this.aaplPrices = result;
        console.log("Most recent AAPL price: " + JSON.stringify(this.aaplPrices))
      })
  }

  private getMsftPrices(){
    this.webService.getPrices("MSFT", 1)
      .subscribe(result => {
        this.msftPrices = result;
        console.log("Most recent MSFT price: " + JSON.stringify(this.msftPrices.price))
      })
  }

  private getGoogPrices(){
    this.webService.getPrices("GOOG", 1)
      .subscribe(result => {
        this.googPrices = result;
        console.log("Most recent AAPL price: " + JSON.stringify(this.googPrices.price))
      })
  }

  private getNSCPrices(){
    this.webService.getPrices("NSC", 1)
      .subscribe(result => {
        this.nscPrices = result;
        console.log("Most recent NSC price: " + JSON.stringify(this.nscPrices.price))
      })
  }

  fillArray(){
    for (let i = 1; i < this.dataPoints.length; i++) {
      if(this.dataPoints[i].stockTicker == "AAPL"){
        this.plotAaplDates.push(this.dataPoints[i].lastStateChange.substring(0,10))
        this.plotAaplPrice.push(this.dataPoints[i].price)
      } else if(this.dataPoints[i].stockTicker == "MSFT") {
        this.plotMsftDates.push(this.dataPoints[i].lastStateChange.substring(0,10))
        this.plotMsftPrice.push(this.dataPoints[i].price)
      } else if(this.dataPoints[i].stockTicker == "GOOG") {
        this.plotGoogDates.push(this.dataPoints[i].lastStateChange.substring(0,10))
        this.plotGoogPrice.push(this.dataPoints[i].price)
      }
    }
  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartAaplLabels = this.plotAaplDates;
  public barChartMsftLabels = this.plotMsftDates;
  public barChartGoogLabels = this.plotGoogDates;
  // public aaplAverageLabels = this.plotAaplAverageDates;
  // public msftAverageLabels = this.msftAaplAverageDates;
  // public googAverageLabels = this.googAaplAverageDates;
  public barChartType = 'line';
  public barChartLegend = true;

  public barChartAaplData = [
    {data: this.plotAaplPrice, label: 'AAPL'}
  ];

  public barChartMsftData = [
    {data: this.plotMsftPrice, label: 'MSFT'}
  ];

  public barChartGoogData = [
    {data: this.plotGoogPrice, label: 'GOOG'}
  ];

}