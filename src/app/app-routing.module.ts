import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Chart1Component } from './chart1/chart1.component';
import { StrategyMovingAveragesComponent } from './strategy-moving-averages/strategy-moving-averages.component';


const routes: Routes = [
  { path: '', component: StrategyMovingAveragesComponent },
  { path: 'analysis', component: Chart1Component },
  { path: '**', component: StrategyMovingAveragesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
