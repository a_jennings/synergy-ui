import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { WebService } from './service/web.service';
import { FormsModule } from '@angular/forms';
import { StocksComponent } from './stocks/stocks.component';
import { ChartsModule } from 'ng2-charts';
import { Chart1Component } from './chart1/chart1.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StrategyMovingAveragesComponent } from './strategy-moving-averages/strategy-moving-averages.component' ;

@NgModule({
  declarations: [
    AppComponent,
    StocksComponent,
    Chart1Component,
    StrategyMovingAveragesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    NgbModule
  ],
  providers: [WebService],
  bootstrap: [AppComponent]
})
export class AppModule { }