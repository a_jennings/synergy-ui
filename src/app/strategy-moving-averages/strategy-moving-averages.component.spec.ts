import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyMovingAveragesComponent } from './strategy-moving-averages.component';

describe('StrategyMovingAveragesComponent', () => {
  let component: StrategyMovingAveragesComponent;
  let fixture: ComponentFixture<StrategyMovingAveragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyMovingAveragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyMovingAveragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
