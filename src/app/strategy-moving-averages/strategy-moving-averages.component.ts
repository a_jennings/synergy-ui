import { Observable, timer, Subscription, VirtualTimeScheduler } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Trade } from '../model/trade';
import { Stock } from '../model/stock';
import { WebService } from '../service/web.service';
import * as CanvasJS from '../../assets/canvasjs.min';
import { ActivatedRoute, Router } from '@angular/router';
import { SimpleStrategy } from '../model/simple_strategy';

@Component({
  selector: 'app-strategy-moving-averages',
  templateUrl: './strategy-moving-averages.component.html',
  styleUrls: ['./strategy-moving-averages.component.css']
})
export class StrategyMovingAveragesComponent implements OnInit {

  private updateSubscription: Subscription;

  trades: Trade[];
  ticker_name:string = 'AAPL'
  size:number= 199
  exit_profit_loss:number= 10
  simpleStrategy: SimpleStrategy;

  constructor(private webService: WebService) { }
    
  ngOnInit() {
    this.updateTrades();
  };

  ngOnDestroy() {
      this.updateSubscription.unsubscribe();
  }

  private updateTrades() {
    this.webService.findAll().subscribe(data => {
      this.trades = data.reverse();
      console.log("loading trades: ");
      console.log(this.trades);
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(10000).subscribe(
      (val) => { this.updateTrades() }
    );
  }

  handleClick(){
    console.log("form entry, ticker: " + this.ticker_name)
    console.log("form entry, size: " + this.size)
    console.log("form entry, exit_profit_loss: " + this.exit_profit_loss)
    this.simpleStrategy = new SimpleStrategy(this.exit_profit_loss, this.size, this.ticker_name);
    this.webService.createSimpleStrategy(this.simpleStrategy).subscribe(
      result => {
        // console.log("Form entry: " + JSON.stringify(result.ticker))
        console.log("this.simpleStrategy: " + JSON.stringify(this.simpleStrategy))
      }
    );
  }

  stopTrades(){
    console.log("button to stop trades")
  }

}
