import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trade } from '../model/trade';
import { Stock } from '../model/stock';
import { Price } from '../model/price';
import { SimpleStrategy } from '../model/simple_strategy';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  private tradeUrl: string;
  private stockUrl: string;
  private simpleStrategyUrl: string;
  private priceUrl: string;

  constructor(private http: HttpClient) {
    this.tradeUrl = environment.rest_host + '/trade';
    this.stockUrl = environment.rest_host + '/stock';
    this.simpleStrategyUrl = environment.rest_host + '/simplestrategy';
    this.priceUrl = environment.rest_host + '/price'
  }
 
  public findAll(): Observable<Trade[]> {
    return this.http.get<Trade[]>(this.tradeUrl);
  }

  public findStocks(): Observable<Stock[]>{
    return this.http.get<Stock[]>(this.stockUrl);
  }

  public save(stock: Stock) {
    console.log("web.service stock object: " + JSON.stringify(stock))
    return this.http.post<Stock>(this.stockUrl, stock);
  }

  public createSimpleStrategy(simpleStrategy: SimpleStrategy){
    return this.http.post<SimpleStrategy>(this.simpleStrategyUrl, simpleStrategy);
  }

  public getPrices(id, count): Observable<Price[]>{
    return this.http.get<Price[]>(`${this.priceUrl}/${id}/${count}`);
  }
}