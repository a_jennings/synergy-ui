export class Trade {
    id: number;
    price: number;
    size: number;
    stockTicker: string;
    lastStateChange: Date;
    tradeType: string;
    state: string;
    constructor(price, size, stockTicker, lastStateChange, tradeType, state){
        this.price = price
        this.size = size
        this.stockTicker = stockTicker
        this.lastStateChange = lastStateChange
        this.tradeType = tradeType
        this.price = price
    }
}