import { Stock } from './stock';

export class SimpleStrategy {

    exit_profit_loss: number;
    size: number;
    stock: Stock;
    
    constructor(exit_profit_loss, size, ticker_name){
        this.size = size
        this.exit_profit_loss = exit_profit_loss
        this.stock = new Stock(ticker_name)
        // this.stock.ticker
    }
}